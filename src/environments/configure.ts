// place code here for configuring or extending application modules
// This file is being replaced during build by using the `fileReplacements` array.

import { NgModuleRef, Type } from '@angular/core';

// example: the following code snippet extends routes by adding an extra route for my-request module

// import { routes } from '../app/app.routing';
// routes.unshift({
//     path: 'requests/my-request',
//     loadChildren: () => import('my-request').then(m => m.MyRequestModule)
// });

export function configure(app: NgModuleRef<any>) {
    //
}
