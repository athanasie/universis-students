import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { template, at } from 'lodash';
import { ProfileService } from '../../../profile/services/profile.service';
import { CurrentRegistrationService } from '../../../registrations/services/currentRegistrationService.service';
import { ConfigurationService } from '@universis/common';

@Component({
  selector: 'app-student-recent-courses',
  templateUrl: './student-recent-courses.component.html',
  styleUrls: ['./student-recent-courses.component.scss']
})
export class StudentRecentCoursesComponent implements OnInit {

  public recentCourses: any = [];
  public currentRegistration: any;
  public isLoading = true;   // Only if data is loaded
  public currentRegistrarionEffectiveStatus: any;
  public defaultLanguage: string | any = "";
  public currentLanguage: string = "";
  public timetableExists = false;

  public registrationEdited = false;

  constructor(private _context: AngularDataContext, private _profileService: ProfileService,
    private _configurationService: ConfigurationService,
    private _currentReg: CurrentRegistrationService) {
    this.currentLanguage = this._configurationService.currentLocale;
    this.defaultLanguage = this._configurationService?.settings?.i18n?.defaultLocale;
  }

  ngOnInit() {
    this._profileService.getStudent().then(student => {
      this.registrationEdited = sessionStorage['registrationEdited'];
      this._context.model('TimetableEvents')
        .where('organizer').equal(student.department.id)
        .and('academicYear').equal(student.department.currentYear.id)
        .and('academicPeriods/id').equal(student.department.currentPeriod.id)
        .and('availableEventTypes/alternateName').equal('TeachingEvent')
        .and('eventStatus/alternateName').equal('EventOpened')
        .getItem()
        .then(timetable => this.timetableExists = !!timetable)

      //  get current registration's effective status
      this._currentReg.getCurrentRegistrationEffectiveStatus().then(effectiveStatus => {
        if (effectiveStatus) {
          this.currentRegistrarionEffectiveStatus = effectiveStatus.status;
        }
        this._currentReg.getCurrentRegistration().then((currentReg: any = {}) => {
          if (currentReg == null) {
            this.currentRegistration = {
              student: student.id,
              registrationYear: student.department.currentYear,
              registrationPeriod: student.department.currentPeriod,
              classes: []
            };
          }
          if (currentReg && currentReg.classes) {
            this.generateClassAndELearningUrls(currentReg.classes).then((res) => {
              this.recentCourses = res;
              this.recentCourses.sort((a, b) => a.courseClass.course.name.localeCompare(b.courseClass.course.name));
              this.currentRegistration = currentReg;
            });
          }
          this.isLoading = false; // Data is loaded
        }).catch(err => {
          if (err.error.statusCode === 404) {
            this.currentRegistration = {
              student: student.id,
              registrationYear: student.department.currentYear,
              registrationPeriod: student.department.currentPeriod,
              classes: []
            };
            this.isLoading = false;
          }
        });
      });
    });
  }
  goToLink(url: string) {
    window.open(url, '_blank');
  }

  async generateClassAndELearningUrls(courses: Array<any>) {
    const student = await this._profileService.getStudent();
    if (student && student.department && student.department.organization && student.department.organization.instituteConfiguration) {
      const instituteConfig = student.department.organization.instituteConfiguration;
      courses.map(course => {
        course.courseClass.classUrl =
          instituteConfig.courseClassUrlTemplate ? template(instituteConfig.courseClassUrlTemplate)(course.courseClass) : '';
        course.courseClass.eLearningUrl =
          instituteConfig.eLearningUrlTemplate ? template(instituteConfig.eLearningUrlTemplate)(course.courseClass) : '';
      });
    }
    return courses;
  }
}
