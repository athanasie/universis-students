import { Component, OnInit, Input } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { GradesService } from '../../services/grades.service';
import { NgChartsModule } from 'ng2-charts';
import { TranslateService } from '@ngx-translate/core';
import { ChartConfiguration } from 'chart.js';

@Component({
  selector: 'app-grades-statbox-bottom',
  templateUrl: './grades-statbox-bottom.component.html',
  styleUrls: ['./grades-statbox-bottom.component.scss']
})
export class GradesStatboxBottomComponent implements OnInit {

  @Input() title?: string;
  @Input() statsPeriod?: string;
  @Input() labelTotal?: string;
  @Input() labelPassed?: string;
  @Input() registeredCourses?: number | any;
  @Input() passedCourses?: number | any;
  @Input() passedGradeWeightedAverage?: string;
  @Input() passedGradeSimpleAverage?: string;
  @Input() totalEcts?: number;
  @Input() registeredEcts?: number;
  @Input() isTranscript?: boolean;
  public failedCourses?: number;
  public doughnutChartData?: number[];
  public doughnutChartType = 'doughnut';
  public doughnutChartLabels: string[] = [];

  constructor(private _contextService: AngularDataContext,
    private _gradeService: GradesService,
    private _translateService: TranslateService) {
  }

  public doughnutChartDatasets: ChartConfiguration<'doughnut'>['data']['datasets'] = [];

  public doughnutChartOptions: ChartConfiguration<'doughnut'>['options'] = {
    responsive: false,
    plugins: {
      legend: {
        display: false
      }
    }
  };

  ngOnInit() {
    this.labelTotal = this.labelTotal ? this.labelTotal : this._translateService.instant('Grades.Statbox.RegisteredCourses');
    this.labelPassed = this.labelPassed ? this.labelPassed : this._translateService.instant('Grades.Statbox.PassedCourses');
    this.failedCourses = this.registeredCourses - this.passedCourses;
    this.doughnutChartData = [this.passedCourses, this.failedCourses];
    this.doughnutChartDatasets = [{ data: this.doughnutChartData, label: "Μαθήματα", backgroundColor: ['#2500dd','#BDB2F5',] }];

    const text = this.passedCourses + '/' + this.registeredCourses;
    this.doughnutChartOptions = Object.assign({
      cutout: "85%",
      legend: {
        display: false
      },
      centerText: {
        display: true,
        text: text
      },
      tooltips: {
        position: 'nearest',
        yAlign: 'bottom'
      }
    });
    this._translateService.get('Grades.PassedCourses').subscribe(x => {
      this.doughnutChartLabels.push(x);
    });
    this._translateService.get('Grades.FailedCourses').subscribe(x => {
      this.doughnutChartLabels.push(x);
    });

  }

}
