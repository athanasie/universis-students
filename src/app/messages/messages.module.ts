import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesListComponent } from './components/messages-list/messages-list.component';
import { MessagesHomeComponent } from './components/messages-home/messages-home.component';
import {MessagesRouting} from './messages.routing';
import {environment} from '../../environments/environment';
import {TranslateModule , TranslateService} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {RequestsModule} from '../requests/requests.module';

import * as el from "./i18n/messages.el.json"
import * as en from "./i18n/messages.en.json"

@NgModule({
  imports: [
    CommonModule,
    MessagesRouting,
    TranslateModule,
    FormsModule,
    RequestsModule,
    InfiniteScrollModule
  ],
  providers: [
  ],
  declarations: [MessagesListComponent, MessagesHomeComponent]
})
export class MessagesModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
}
}
