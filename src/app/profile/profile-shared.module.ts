import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {environment} from '../../environments/environment';
import {ProfileCardComponent} from './components/profile-card/profile-card.component';
import {SharedModule} from '@universis/common';
import {ProfileService} from './services/profile.service';
import {StudentsSharedModule} from '../students-shared/students-shared.module';

import * as el from "./i18n/profile.el.json"
import * as en from "./i18n/profile.en.json"

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        MostModule,
        StudentsSharedModule.forRoot()
    ],
    declarations: [
        ProfileCardComponent
    ],
    exports: [
        ProfileCardComponent
    ],
    providers: [
        ProfileService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfileSharedModule {

    constructor(private _translateService: TranslateService) {
        this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
    }
}
