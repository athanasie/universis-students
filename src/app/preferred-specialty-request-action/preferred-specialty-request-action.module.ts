import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreferredSpecialtyRequestActionRoutingModule } from './preferred-specialty-request-action-routing.module';
import { ApplyComponent } from './apply/apply.component';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { AdvancedFormsModule } from '@universis/forms';
import { PreviewComponent } from './preview/preview.component';

import * as el from './i18n/preferred-specialty-request-action.el.json';
import * as en from './i18n/preferred-specialty-request-action.en.json';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    AdvancedFormsModule,
    PreferredSpecialtyRequestActionRoutingModule,
  ],
  declarations: [ApplyComponent, PreviewComponent],
})
export class PreferredSpecialtyRequestActionModule {
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch((err) => {
      console.error(
        'An error occurred while loading PreferredSpecialtyRequestActionModule.'
      );
      console.error(err);
    });
  }

  // tslint:disable-next-line: use-life-cycle-interface
  async ngOnInit() {
    this._translateService.setTranslation('el', el, true);
    this._translateService.setTranslation('en', en, true);
  }
}
